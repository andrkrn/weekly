import Ember from 'ember';
/* global key */

export default Ember.Component.extend({
  didInsertElement: function() {
    var heading_element = $('.js-heading-keymaster');
    key('ctrl+a', function(e) {
      e.preventDefault();
      heading_element.text('event `ctrl + a` fired!');
    });

    key('alt+a, command+a', 'lol', function(e) {
      e.preventDefault();
      heading_element.text('Back to Sample Keymaster');
    });

    var binding_event = function() {
      key('a', function() {
        key('b', function() {
          heading_element.text('Clicked a then b');
        });
      });
    };

    // First time load
    binding_event();
    setInterval(function() {
      key.unbind('b');
      key.unbind('a');
      binding_event();
    }, 2000);

    key('f1', function() {
      key.setScope('lol');
      console.log('Keymaster now has scope `lol`');
      return false;
    });

    key('f2', function() {
      key.setScope('all');
      console.log('Default scope');
      return false;
    });
  }
});
